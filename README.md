# Selenium Python Docker

An image to run dockerized Python Selenium applications.

You can pull the built image by using `docker pull micahparks/seleniumpython` or build it for yourself with `./build.sh`.