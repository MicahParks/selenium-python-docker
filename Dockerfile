FROM python
RUN apt update && \
    apt full-upgrade -y && \
    apt install firefox-esr xvfb -y && \
    wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz -O geckodriver.tar.gz && \
    tar xzf geckodriver.tar.gz && \
    rm geckodriver.tar.gz && \
    mv geckodriver /usr/bin/
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt && rm requirements.txt
